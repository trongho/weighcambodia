﻿using Neodynamic.SDK.Printing;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeighingApp.Models;
using WeighingApp.Repositorys;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace WeighingApp
{
    public partial class MainForm : DevExpress.XtraBars.ToolbarForm.ToolbarForm
    {
        MaterialRepository materialRepository = null;
        ProductRepository productRepository = null;
        WeighingRepository weighingRepository = null;
        internal List<Material> materialsPublic = null;
        internal List<Product> productsPublic = null;
        internal List<Weighing> weighingsPublic = null;
        Timer t = new Timer();
        AutoCompleteStringCollection coll = null;
        AutoCompleteStringCollection coll2 = null;

        int weightIndex = 0;
        int printCount = 0;
        internal bool serverStatus = false;
        internal bool updateFromSQlClick = false;
        internal bool updateFromExcelClick = false;
        bool fileExist = true;
        public MainForm()
        {
            InitializeComponent();
            this.Load += MainForm_Load;
        }

        private void MainForm_Load(Object sender, EventArgs e)
        {
            materialRepository = new MaterialRepository();
            productRepository = new ProductRepository();
            weighingRepository = new WeighingRepository();
            weighingsPublic = new List<Weighing>();

            this.textEditProductLN.KeyDown += new KeyEventHandler(ProductLN_KeyDown);
            this.textEditMaterialLN.KeyDown += new KeyEventHandler(MaterialLN_KeyDown);
            this.barButtonItemUpdate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barButtonItemUpdate_ItemClickAsync);
            this.barButtonItemUpdateFromSQL.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barButtonItemUpdateFromSQL_ItemClickAsync);
            this.barButtonItemImportData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(barButtonItemImportData_ItemClickAsync);
            this.simpleButtonTare.Click += simpleButtonTare_Click;
            this.simpleButtonAllow.Click += simpleButtonAllow_Click;
            this.simpleButtonNW.Click += simpleButtonNW_Click;
            this.simpleButtonListSQL.Click += simpleButtonListSQL_Click;
            this.simpleButtonRecordList.Click += simpleButtonRecordList_Click;
            this.simpleButtonContinue.Click += simpleButtonContinue_Click;
            this.simpleButtonClear.Click += simpleButtonClear_Click;
            this.simpleButtonPrint.Click += simpleButtonPrint_ClickAsync;

            this.textEditProductLN.TextChanged += txtPenaltyDays_TextChanged;

            t.Tick += new EventHandler(this.Timer_Tick);
            t.Start();




        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            checkServerConnectionStatusAsync();

            if (textEditProductLN.Text != "" &&
            textEditProductName.Text != "" &&
            textEditProductExpDate.Text != "" &&
            textEditMaterialLN.Text != "" &&
            textEditMaterialName.Text != "" &&
            textEditMaterialCode.Text != "" &&
            textEditMaterialExpDate.Text != "" &&
            textEditTare.Text != "" &&
            textEditNetWeight.Text != "" &&
            textEditGrossWeight.Text != "" &&
            textEditWeighingBy.Text != "" &&
            textEditWeighingDate.Text != "" &&
            textEditBalanceCode.Text != "")
            {
                simpleButtonPrint.Enabled = true;
            }
        }

        private async Task checkServerConnectionStatusAsync()
        {
            List<Product> products = await productRepository.GetAll();
            if (products.Count == 0)
            {
                serverStatus = false;
                barHeaderItemServerConnectionStatus.Caption = "Server Offline";
            }
            else
            {
                serverStatus = true;
                barHeaderItemServerConnectionStatus.Caption = "Server Online";
            }
        }

        internal void AutoCompleteProductLotNo(List<Product> productList)
        {
            coll = new AutoCompleteStringCollection();
            foreach (Product product in productList)
            {
                if (product.LotNo != "")
                {
                    coll.Add(product.LotNo);
                }
            }
            var tx = textEditProductLN.MaskBox;
            tx.AutoCompleteMode = AutoCompleteMode.Suggest;
            tx.AutoCompleteSource = AutoCompleteSource.CustomSource;
            tx.AutoCompleteCustomSource = coll;
        }

        internal void AutoCompleteMaterialLotNo(List<Material> materialList)
        {
            coll2 = new AutoCompleteStringCollection();
            foreach (Material material in materialList)
            {
                if (material.LotNo.ToString() != "")
                {
                    coll2.Add(material.LotNo.ToString());
                }
            }
            var tx2 = textEditMaterialLN.MaskBox;
            tx2.AutoCompleteSource = AutoCompleteSource.CustomSource;
            tx2.AutoCompleteMode = AutoCompleteMode.Suggest;
            tx2.AutoCompleteCustomSource = coll2;
        }

        private void txtPenaltyDays_TextChanged(object sender, EventArgs e)
        {

        }


        private void ProductLN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                //Product product = new Product();
                //product = await productRepository.GetUnderID(textEditProductLN.Text);

                if (productsPublic != null)
                {
                    foreach (Product product in productsPublic)
                    {
                        if (textEditProductLN.Text.Trim().Equals(product.LotNo))
                        {
                            textEditProductName.Text = product.ProductName;
                            textEditProductExpDate.Text = product.ExpDate.ToString().Substring(0, product.ExpDate.ToString().IndexOf(" "));
                            break;
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Not Update Database Yet!!!!");
                    return;
                }
            }
        }

        private void MaterialLN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //Material material = new Material();
                //material = await materialRepository.GetUnderID(textEditMaterialLN.Text);

                if (materialsPublic != null)
                {
                    foreach (Material material in materialsPublic)
                    {
                        if (material.LotNo.Equals(textEditMaterialLN.Text))
                        {
                            textEditMaterialName.Text = material.MaterialName;
                            textEditMaterialExpDate.Text = material.ExpDate.ToString().Substring(0, material.ExpDate.ToString().IndexOf(" "));
                            textEditMaterialCode.Text = material.Code;
                            break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Not Update Databse Yet!!!!");
                    return;
                }
            }
        }

        private async void barButtonItemUpdate_ItemClickAsync(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            updateFromExcelClick = true;
            updateFromSQlClick = false;

            materialsPublic = await ExcelToListMaterial();
            productsPublic = await ExcelToListProduct();

            if (fileExist == false)
            {
                MessageBox.Show("Can not fetch data. Excel File Is Not Exist!!!");
                return;
            }

            MessageBox.Show("Fetch data from excel file success!!!");


            if (productsPublic != null)
            {
                AutoCompleteProductLotNo(productsPublic);
            }
            if (materialsPublic != null)
            {
                AutoCompleteMaterialLotNo(materialsPublic);
            }
        }

        private async void barButtonItemUpdateFromSQL_ItemClickAsync(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (serverStatus == false)
            {
                MessageBox.Show("Can not fetch data. Server is offline!!!");
                return;
            }

            updateFromExcelClick = false;
            updateFromSQlClick = true;

            materialsPublic = await materialRepository.GetAll();
            productsPublic = await productRepository.GetAll();
            MessageBox.Show("Fetch data from sql script success!!!");

            if (productsPublic != null)
            {
                AutoCompleteProductLotNo(productsPublic);
            }
            if (materialsPublic != null)
            {
                AutoCompleteMaterialLotNo(materialsPublic);
            }

        }

        private void simpleButtonAllow_Click(object sender, EventArgs e)
        {
            simpleButtonPrint.Enabled = true;
            MessageBox.Show("Enable print button!!!!!!!!");
        }

        private void simpleButtonTare_Click(object sender, EventArgs e)
        {
            if (textEditProductLN.Text == "" &&
            textEditProductName.Text == "" &&
            textEditProductExpDate.Text == "" &&
            textEditMaterialLN.Text == "" &&
            textEditMaterialName.Text == "" &&
            textEditMaterialCode.Text == "" &&
            textEditMaterialExpDate.Text == "")
            {
                MessageBox.Show("Please fill data above!!!!!");
                return;
            }

            Random rnd = new Random();
            int tare = rnd.Next(1, 200);
            textEditTare.Text = tare + "";

            string[] balanceCodes = { "Scale A", "Scale B", "Scale C" };
            int index = rnd.Next(balanceCodes.Length);
            textEditBalanceCode.Text = balanceCodes[index];
        }

        private void simpleButtonNW_Click(object sender, EventArgs e)
        {
            if (
                textEditTare.Text == "")
            {
                MessageBox.Show("Please put bottle and push [Tare] button on weigh and application!!!!!");
                return;
            }

            Random rnd = new Random();
            int netWeight = rnd.Next(1000, 2000);
            int grossWeight = int.Parse(textEditTare.Text) + netWeight;
            textEditNetWeight.Text = netWeight + "";
            textEditGrossWeight.Text = grossWeight + "";
            textEditWeighingDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        private void simpleButtonListSQL_Click(object sender, EventArgs e)
        {
            ListSQLForm form = new ListSQLForm(this);
            form.ShowDialog(this);
            form.Dispose();
        }

        private void simpleButtonRecordList_Click(object sender, EventArgs e)
        {
            RecordListForm form = new RecordListForm(this);
            form.ShowDialog(this);
            form.Dispose();
        }

        private void simpleButtonContinue_Click(object sender, EventArgs e)
        {
            textEditTare.Text = "";
            textEditNetWeight.Text = "";
            textEditGrossWeight.Text = "";
        }

        private void simpleButtonClear_Click(object sender, EventArgs e)
        {
            textEditProductLN.Text = "";
            textEditProductName.Text = "";
            textEditProductExpDate.Text = "";
            textEditMaterialLN.Text = "";
            textEditMaterialName.Text = "";
            textEditMaterialCode.Text = "";
            textEditMaterialExpDate.Text = "";
            textEditTare.Text = "";
            textEditNetWeight.Text = "";
            textEditGrossWeight.Text = "";
            textEditWeighingBy.Text = "";
            textEditWeighingDate.Text = "";
            textEditBalanceCode.Text = "";
        }

        private async void simpleButtonPrint_ClickAsync(object sender, EventArgs e)
        {
            if (serverStatus == false)
            {
                Weighing weighing = new Weighing();
                weighing.WeighingID = (weightIndex + 1).ToString();
                weighing.ProductLN = textEditProductLN.Text;
                weighing.ProductName = textEditProductName.Text;
                weighing.ProductExpDate = textEditProductExpDate.Text!=""?DateTime.Parse(textEditProductExpDate.Text): (DateTime?)null;
                weighing.MaterialLN = textEditMaterialLN.Text;
                weighing.MaterialName = textEditMaterialName.Text;
                weighing.MaterialExpDate = textEditMaterialExpDate.Text != "" ? DateTime.Parse(textEditMaterialExpDate.Text) : (DateTime?)null;
                weighing.MaterialCode = textEditMaterialCode.Text;
                weighing.Tare = textEditTare.Text!=""? decimal.Parse(textEditTare.Text):0;
                weighing.NetWeight = textEditNetWeight.Text != "" ? decimal.Parse(textEditNetWeight.Text) : 0;
                weighing.GrossWeight = textEditGrossWeight.Text != "" ? decimal.Parse(textEditGrossWeight.Text) : 0;
                weighing.WeighingBy = textEditWeighingBy.Text;
                weighing.WeighingDate = textEditWeighingDate.Text != "" ? DateTime.Parse(textEditWeighingDate.Text) : (DateTime?)null;
                weighing.BalanceCode = textEditBalanceCode.Text;
                weighingsPublic.Add(weighing);
                weightIndex++;
                printCount++;

                //print(weighing);

                MessageBox.Show("Print success " + printCount + " label");
                simpleButtonPrint.Enabled = false;
            }
            else
            {
                Weighing weighing = new Weighing();
                weighing.WeighingID = await weighingRepository.GetID();
                weighing.ProductLN = textEditProductLN.Text;
                weighing.ProductName = textEditProductName.Text;
                weighing.ProductExpDate = textEditProductExpDate.Text != "" ? DateTime.Parse(textEditProductExpDate.Text) : (DateTime?)null;
                weighing.MaterialLN = textEditMaterialLN.Text;
                weighing.MaterialName = textEditMaterialName.Text;
                weighing.MaterialExpDate = textEditMaterialExpDate.Text != "" ? DateTime.Parse(textEditMaterialExpDate.Text) : (DateTime?)null;
                weighing.MaterialCode = textEditMaterialCode.Text;
                weighing.Tare = textEditTare.Text != "" ? decimal.Parse(textEditTare.Text) : 0;
                weighing.NetWeight = textEditNetWeight.Text != "" ? decimal.Parse(textEditNetWeight.Text) : 0;
                weighing.GrossWeight = textEditGrossWeight.Text != "" ? decimal.Parse(textEditGrossWeight.Text) : 0;
                weighing.WeighingBy = textEditWeighingBy.Text;
                weighing.WeighingDate = textEditWeighingDate.Text != "" ? DateTime.Parse(textEditWeighingDate.Text) : (DateTime?)null;
                weighing.BalanceCode = textEditBalanceCode.Text;
                weighingsPublic.Add(weighing);
                await weighingRepository.Create(weighing);

                printCount++;

                //print(weighing);

                MessageBox.Show(weighing.WeighingID+"");

                MessageBox.Show("Print success " + printCount + " label");
                simpleButtonPrint.Enabled = false;
            }

            

        }

        private async Task<List<Material>> ExcelToListMaterial()
        {
            FileInfo file = new FileInfo("d:\\Stock controll.xlsx");
            List<Material> materials = new List<Material>();

            if (file.Exists == false)
            {
                fileExist = false;
            }
            else
            {
                fileExist = true;
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage package = new ExcelPackage(file))
                {

                    ExcelWorksheet workSheet = package.Workbook.Worksheets["RM+PM PPM"];

                    int totalRows = workSheet.Dimension.Rows;
                    for (int i = 9; i <= totalRows; i++)
                    {
                        if (workSheet?.Cells[i, 9].Text.Trim() != "")
                        {
                            materials.Add(new Material
                            {
                                LotNo = workSheet?.Cells[i, 8].Text.Trim(),
                                Code = workSheet?.Cells[i, 7].Text.Trim(),
                                MaterialName = workSheet?.Cells[i, 4].Text.Trim(),
                                ExpDate = Convert.ToDateTime(workSheet?.Cells[i, 9].Text.Trim()),
                            });
                        }
                        else
                        {
                            materials.Add(new Material
                            {
                                LotNo =workSheet?.Cells[i, 8].Text.Trim(),
                                Code = workSheet?.Cells[i, 7].Text.Trim(),
                                MaterialName = workSheet?.Cells[i, 4].Text.Trim(),
                            });
                        }

                    }
                }
            }
            return materials;
        }

        private async Task<List<Product>> ExcelToListProduct()
        {
            FileInfo file = new FileInfo("d:\\Stock controll.xlsx");
            List<Product> products = new List<Product>();
            if (file.Exists == false)
            {
                fileExist = false;
            }
            else
            {
                fileExist = true;
                ExcelPackage.LicenseContext = LicenseContext.Commercial;
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage package = new ExcelPackage(file))
                {

                    ExcelWorksheet workSheet = package.Workbook.Worksheets["RM+PM PPM"];

                    int totalRows = workSheet.Dimension.Rows;
                    for (int i = 9; i <= totalRows; i++)
                    {
                        if (workSheet?.Cells[i, 9].Text.Trim() != "")
                        {
                            products.Add(new Product
                            {
                                LotNo =workSheet?.Cells[i, 8].Text.Trim(),
                                ProductName = workSheet?.Cells[i, 4].Text.Trim(),
                                ExpDate = Convert.ToDateTime(workSheet?.Cells[i, 9].Text.Trim()),
                            });
                        }
                        else
                        {
                            products.Add(new Product
                            {
                                LotNo = workSheet?.Cells[i, 8].Text.Trim(),
                                ProductName = workSheet?.Cells[i, 4].Text.Trim(),
                            });
                        }

                    }
                }
            }
            return products;
        }

        private async Task<bool> exportExcel(List<Weighing> weighings)
        {
            FileInfo file = new FileInfo("d:\\weighing-export.xlsx");
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = weighings.Count;
                int i = 0;
                for (int row = 3; row <= totalRows + 4; row++)
                {
                    worksheet.Cells[row, 2].Value = weighings[i].WeighingID;
                    i++;
                }
                package.Save();

            }
            return true;
        }

        private void CheckExcel()
        {
            FileInfo file = new FileInfo("d:\\Stock controll.xlsx");

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets["Test"];
                string tareScaleA = workSheet?.Cells[3, 1].Text.Trim();
                string netWeightScaleA = workSheet?.Cells[3, 2].Text.Trim();
                string tareScaleB = workSheet?.Cells[3, 3].Text.Trim();
                string netWeightScaleB = workSheet?.Cells[3, 4].Text.Trim();
                string tareScaleC = workSheet?.Cells[3, 5].Text.Trim();
                string netWeightScaleC = workSheet?.Cells[3, 6].Text.Trim();

                if (tareScaleA != null && tareScaleA != "")
                {
                    textEditBalanceCode.Text = "Scale A";
                }
                else if (tareScaleB != null && tareScaleB != "")
                {
                    textEditBalanceCode.Text = "Scale B";
                }
                else if (tareScaleC != null && tareScaleC != "")
                {
                    textEditBalanceCode.Text = "Scale C";
                }
                else
                {
                    textEditBalanceCode.Text = "";
                }
            }
        }

        private async void barButtonItemImportData_ItemClickAsync(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PrintForm form = new PrintForm();
            form.ShowDialog(this);
            form.Dispose();

            //FileInfo file = new FileInfo("d:\\Stock controll.xlsx");

            //ExcelPackage.LicenseContext = LicenseContext.Commercial;
            //ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            //using (ExcelPackage package = new ExcelPackage(file))
            //{

            //    ExcelWorksheet workSheet = package.Workbook.Worksheets["RM+PM PPM"];
            //    int totalRows = workSheet.Dimension.Rows;

            //    List<Product> products = new List<Product>();
            //    for (int i = 9; i <= totalRows; i++)
            //    {
            //        if (workSheet.Cells[i, 8].Text.Trim() != "")
            //        {
            //            products.Add(new Product
            //            {
            //                LotNo = workSheet?.Cells[i, 8].Text.Trim(),
            //                ProductName = workSheet?.Cells[i, 4].Text.Trim(),
            //                ExpDate = Convert.ToDateTime(workSheet?.Cells[i, 9].Text.Trim()),
            //            });
            //        }
            //    }

            //    List<Material> materials = new List<Material>();
            //    for (int i = 9; i <= totalRows; i++)
            //    {
            //        if (workSheet.Cells[i, 8].Text.Trim() != "")
            //        {
            //            materials.Add(new Material
            //            {
            //                LotNo = workSheet?.Cells[i, 8].Text.Trim(),
            //                Code = workSheet?.Cells[i, 7].Text.Trim(),
            //                MaterialName = workSheet?.Cells[i, 4].Text.Trim(),
            //                ExpDate = Convert.ToDateTime(workSheet?.Cells[i, 9].Text.Trim()),
            //            });
            //        }
            //    }

            //    foreach (Product product in products)
            //    {
            //        await productRepository.Create(product);
            //    }

            //    foreach (Material material in materials)
            //    {
            //        await materialRepository.Create(material);
            //    }

            //    MessageBox.Show("Success add!!!!!!!!!");
            //}
        }

        private void print(Weighing weighing)
        {
            //Define a ThermalLabel object and set unit to inch and label size
            ThermalLabel tLabel = new ThermalLabel(UnitType.Inch, 4, 17);
            tLabel.GapLength = 0.2;

            //Define a **BarcodeItem** object for encoding Code 39 symbology
            BarcodeItem bc1 = new BarcodeItem(0.2, 0.2, 2, 1, BarcodeSymbology.Code39,weighing.WeighingID.ToString());
            bc1.AddChecksum = false;
            bc1.CodeAlignment = BarcodeTextAlignment.BelowJustify;
            bc1.BarWidth = 0.01;
            bc1.BarHeight = 0.5;
            bc1.QuietZone = new FrameThickness(0);
            bc1.BarcodeAlignment = BarcodeAlignment.MiddleCenter;
            bc1.BorderThickness = new FrameThickness(0.03);

            //A sample text using NativePrinterFontA font            
            TextItem txt1 = new TextItem(2,2.25, 3.5, 0.5, weighing.ProductLN);
            txt1.TextAlignment = TextAlignment.Center;
            txt1.Font.Name = Neodynamic.SDK.Printing.Font.NativePrinterFontA;
            txt1.Font.Unit = FontUnit.Point;
            txt1.Font.Size = 10;

            //A sample text using NativePrinterFontA font            
            TextItem txt2 = new TextItem(2, 3.25, 3.5, 0.5, weighing.ProductName);
            txt1.TextAlignment = TextAlignment.Center;
            txt1.Font.Name = Neodynamic.SDK.Printing.Font.NativePrinterFontA;
            txt1.Font.Unit = FontUnit.Point;
            txt1.Font.Size = 10;

            //A sample text using NativePrinterFontA font            
            TextItem txt3= new TextItem(2, 4.25, 3.5, 0.5, weighing.ProductExpDate.ToString());
            txt1.TextAlignment = TextAlignment.Center;
            txt1.Font.Name = Neodynamic.SDK.Printing.Font.NativePrinterFontA;
            txt1.Font.Unit = FontUnit.Point;
            txt1.Font.Size = 10;


            //Add items to ThermalLabel object...
            tLabel.Items.Add(bc1);
            tLabel.Items.Add(txt1);
            tLabel.Items.Add(txt2);
            tLabel.Items.Add(txt3);

            //Create a WindowsPrintJob object
            using (WindowsPrintJob pj = new WindowsPrintJob())
            {
                //Create PrinterSettings object
                PrinterSettings myPrinter = new PrinterSettings();
                myPrinter.Communication.CommunicationType = CommunicationType.Network;
                myPrinter.Communication.NetworkIPAddress = System.Net.IPAddress.Parse("127.0.0.1");
                myPrinter.Communication.NetworkPort = 9100;
                myPrinter.Dpi = 203;
                myPrinter.ProgrammingLanguage = ProgrammingLanguage.ZPL;
                myPrinter.PrinterName = "Zpl Printer";

                //Set PrinterSettings to WindowsPrintJob
                pj.PrinterSettings = myPrinter;
                //Print ThermalLabel object...
                pj.Print(tLabel);
            }
        }
    }
}
