﻿
namespace WeighingApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.XtraLayout.ColumnDefinition columnDefinition38 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition17 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition18 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition19 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition20 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition21 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition36 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition37 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition16 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition34 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition35 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition14 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition15 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition32 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition33 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition13 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition39 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition40 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition41 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition42 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition43 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition44 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition45 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition46 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition22 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition24 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition25 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition26 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition27 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition28 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition29 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition30 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition31 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition12 = new DevExpress.XtraLayout.RowDefinition();
            this.toolbarFormControl1 = new DevExpress.XtraBars.ToolbarForm.ToolbarFormControl();
            this.toolbarFormManager1 = new DevExpress.XtraBars.ToolbarForm.ToolbarFormManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItemUpdate = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUpdateFromSQL = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemImportData = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barHeaderItemServerConnectionStatus = new DevExpress.XtraBars.BarHeaderItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditProductName = new DevExpress.XtraEditors.TextEdit();
            this.textEditProductExpDate = new DevExpress.XtraEditors.TextEdit();
            this.textEditMaterialName = new DevExpress.XtraEditors.TextEdit();
            this.textEditMaterialCode = new DevExpress.XtraEditors.TextEdit();
            this.textEditProductLN = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditMaterialLN = new DevExpress.XtraEditors.TextEdit();
            this.textEditMaterialExpDate = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditGrossWeight = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditTare = new DevExpress.XtraEditors.TextEdit();
            this.textEditNetWeight = new DevExpress.XtraEditors.TextEdit();
            this.textEditWeighingBy = new DevExpress.XtraEditors.TextEdit();
            this.textEditWeighingDate = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.textEditBalanceCode = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl6 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControl7 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl8 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButtonAllow = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonTare = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonNW = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonListSQL = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonRecordList = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonContinue = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonClear = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonPrint = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.toolbarFormControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolbarFormManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductExpDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMaterialName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMaterialCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductLN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMaterialLN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMaterialExpDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditGrossWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTare.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNetWeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWeighingBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWeighingDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditBalanceCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).BeginInit();
            this.layoutControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl8)).BeginInit();
            this.layoutControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            this.SuspendLayout();
            // 
            // toolbarFormControl1
            // 
            this.toolbarFormControl1.Location = new System.Drawing.Point(0, 0);
            this.toolbarFormControl1.Manager = this.toolbarFormManager1;
            this.toolbarFormControl1.Name = "toolbarFormControl1";
            this.toolbarFormControl1.Size = new System.Drawing.Size(1201, 31);
            this.toolbarFormControl1.TabIndex = 0;
            this.toolbarFormControl1.TabStop = false;
            this.toolbarFormControl1.TitleItemLinks.Add(this.barButtonItemUpdate);
            this.toolbarFormControl1.TitleItemLinks.Add(this.barButtonItemUpdateFromSQL);
            this.toolbarFormControl1.TitleItemLinks.Add(this.barButtonItemImportData);
            this.toolbarFormControl1.TitleItemLinks.Add(this.barHeaderItemServerConnectionStatus);
            this.toolbarFormControl1.ToolbarForm = this;
            // 
            // toolbarFormManager1
            // 
            this.toolbarFormManager1.DockControls.Add(this.barDockControlTop);
            this.toolbarFormManager1.DockControls.Add(this.barDockControlBottom);
            this.toolbarFormManager1.DockControls.Add(this.barDockControlLeft);
            this.toolbarFormManager1.DockControls.Add(this.barDockControlRight);
            this.toolbarFormManager1.Form = this;
            this.toolbarFormManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemUpdate,
            this.barButtonItemUpdateFromSQL,
            this.barButtonItemImportData,
            this.barButtonItem1,
            this.barHeaderItemServerConnectionStatus});
            this.toolbarFormManager1.MaxItemId = 5;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 31);
            this.barDockControlTop.Manager = this.toolbarFormManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1201, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 629);
            this.barDockControlBottom.Manager = this.toolbarFormManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1201, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Manager = this.toolbarFormManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 598);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1201, 31);
            this.barDockControlRight.Manager = this.toolbarFormManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 598);
            // 
            // barButtonItemUpdate
            // 
            this.barButtonItemUpdate.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItemUpdate.Caption = "Update From Excel";
            this.barButtonItemUpdate.Id = 0;
            this.barButtonItemUpdate.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemUpdate.ImageOptions.SvgImage")));
            this.barButtonItemUpdate.Name = "barButtonItemUpdate";
            this.barButtonItemUpdate.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItemUpdateFromSQL
            // 
            this.barButtonItemUpdateFromSQL.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItemUpdateFromSQL.Caption = "Update From SQL";
            this.barButtonItemUpdateFromSQL.Id = 1;
            this.barButtonItemUpdateFromSQL.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemUpdateFromSQL.ImageOptions.SvgImage")));
            this.barButtonItemUpdateFromSQL.Name = "barButtonItemUpdateFromSQL";
            this.barButtonItemUpdateFromSQL.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItemImportData
            // 
            this.barButtonItemImportData.Caption = "barButtonItem1";
            this.barButtonItemImportData.Id = 2;
            this.barButtonItemImportData.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("barButtonItemImportData.ImageOptions.SvgImage")));
            this.barButtonItemImportData.Name = "barButtonItemImportData";
            this.barButtonItemImportData.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 3;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barHeaderItemServerConnectionStatus
            // 
            this.barHeaderItemServerConnectionStatus.Caption = "Server Connection Status";
            this.barHeaderItemServerConnectionStatus.Id = 4;
            this.barHeaderItemServerConnectionStatus.Name = "barHeaderItemServerConnectionStatus";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEditProductName);
            this.layoutControl1.Controls.Add(this.textEditProductExpDate);
            this.layoutControl1.Controls.Add(this.textEditMaterialName);
            this.layoutControl1.Controls.Add(this.textEditMaterialCode);
            this.layoutControl1.Controls.Add(this.textEditProductLN);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 31);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(641, 144, 855, 545);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1201, 240);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEditProductName
            // 
            this.textEditProductName.Enabled = false;
            this.textEditProductName.Location = new System.Drawing.Point(286, 17);
            this.textEditProductName.MenuManager = this.toolbarFormManager1;
            this.textEditProductName.Name = "textEditProductName";
            this.textEditProductName.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditProductName.Properties.Appearance.Options.UseForeColor = true;
            this.textEditProductName.Properties.AutoHeight = false;
            this.textEditProductName.Size = new System.Drawing.Size(898, 30);
            this.textEditProductName.StyleController = this.layoutControl1;
            this.textEditProductName.TabIndex = 4;
            // 
            // textEditProductExpDate
            // 
            this.textEditProductExpDate.Enabled = false;
            this.textEditProductExpDate.Location = new System.Drawing.Point(286, 105);
            this.textEditProductExpDate.MenuManager = this.toolbarFormManager1;
            this.textEditProductExpDate.Name = "textEditProductExpDate";
            this.textEditProductExpDate.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditProductExpDate.Properties.Appearance.Options.UseForeColor = true;
            this.textEditProductExpDate.Properties.AutoHeight = false;
            this.textEditProductExpDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.textEditProductExpDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEditProductExpDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.textEditProductExpDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEditProductExpDate.Size = new System.Drawing.Size(898, 30);
            this.textEditProductExpDate.StyleController = this.layoutControl1;
            this.textEditProductExpDate.TabIndex = 6;
            // 
            // textEditMaterialName
            // 
            this.textEditMaterialName.Enabled = false;
            this.textEditMaterialName.Location = new System.Drawing.Point(286, 149);
            this.textEditMaterialName.MenuManager = this.toolbarFormManager1;
            this.textEditMaterialName.Name = "textEditMaterialName";
            this.textEditMaterialName.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditMaterialName.Properties.Appearance.Options.UseForeColor = true;
            this.textEditMaterialName.Properties.AutoHeight = false;
            this.textEditMaterialName.Size = new System.Drawing.Size(898, 30);
            this.textEditMaterialName.StyleController = this.layoutControl1;
            this.textEditMaterialName.TabIndex = 7;
            // 
            // textEditMaterialCode
            // 
            this.textEditMaterialCode.Enabled = false;
            this.textEditMaterialCode.Location = new System.Drawing.Point(286, 193);
            this.textEditMaterialCode.MenuManager = this.toolbarFormManager1;
            this.textEditMaterialCode.Name = "textEditMaterialCode";
            this.textEditMaterialCode.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditMaterialCode.Properties.Appearance.Options.UseForeColor = true;
            this.textEditMaterialCode.Properties.AutoHeight = false;
            this.textEditMaterialCode.Size = new System.Drawing.Size(898, 30);
            this.textEditMaterialCode.StyleController = this.layoutControl1;
            this.textEditMaterialCode.TabIndex = 8;
            // 
            // textEditProductLN
            // 
            this.textEditProductLN.Location = new System.Drawing.Point(286, 61);
            this.textEditProductLN.MenuManager = this.toolbarFormManager1;
            this.textEditProductLN.Name = "textEditProductLN";
            this.textEditProductLN.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditProductLN.Properties.Appearance.Options.UseForeColor = true;
            this.textEditProductLN.Properties.AutoHeight = false;
            this.textEditProductLN.Size = new System.Drawing.Size(898, 30);
            this.textEditProductLN.StyleController = this.layoutControl1;
            this.textEditProductLN.TabIndex = 9;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem2});
            this.Root.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.Root.Name = "Root";
            columnDefinition38.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition38.Width = 100D;
            this.Root.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition38});
            rowDefinition17.Height = 20D;
            rowDefinition17.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition18.Height = 20D;
            rowDefinition18.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition19.Height = 20D;
            rowDefinition19.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition20.Height = 20D;
            rowDefinition20.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition21.Height = 20D;
            rowDefinition21.SizeType = System.Windows.Forms.SizeType.Percent;
            this.Root.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition17,
            rowDefinition18,
            rowDefinition19,
            rowDefinition20,
            rowDefinition21});
            this.Root.Size = new System.Drawing.Size(1201, 240);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.textEditProductName;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1181, 44);
            this.layoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem1.Text = " ានឹងចាប់យកច្មោះកនុយត្បអប/ For Producing:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(266, 17);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.textEditProductExpDate;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 88);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem3.Size = new System.Drawing.Size(1181, 44);
            this.layoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem3.Text = "Exp.date:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(266, 16);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.textEditMaterialName;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 132);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem4.Size = new System.Drawing.Size(1181, 44);
            this.layoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem4.Text = "Material name:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(266, 16);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.textEditMaterialCode;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 176);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem5.Size = new System.Drawing.Size(1181, 44);
            this.layoutControlItem5.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem5.Text = "Code:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(266, 16);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.textEditProductLN;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 44);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem2.Size = new System.Drawing.Size(1181, 44);
            this.layoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem2.Text = "Lot No";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(266, 16);
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.textEditMaterialLN);
            this.layoutControl2.Controls.Add(this.textEditMaterialExpDate);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl2.Location = new System.Drawing.Point(0, 271);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(932, 67, 650, 400);
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1201, 66);
            this.layoutControl2.TabIndex = 2;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // textEditMaterialLN
            // 
            this.textEditMaterialLN.Location = new System.Drawing.Point(69, 17);
            this.textEditMaterialLN.MenuManager = this.toolbarFormManager1;
            this.textEditMaterialLN.Name = "textEditMaterialLN";
            this.textEditMaterialLN.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditMaterialLN.Properties.Appearance.Options.UseForeColor = true;
            this.textEditMaterialLN.Properties.AutoHeight = false;
            this.textEditMaterialLN.Size = new System.Drawing.Size(524, 32);
            this.textEditMaterialLN.StyleController = this.layoutControl2;
            this.textEditMaterialLN.TabIndex = 4;
            // 
            // textEditMaterialExpDate
            // 
            this.textEditMaterialExpDate.Location = new System.Drawing.Point(659, 17);
            this.textEditMaterialExpDate.MenuManager = this.toolbarFormManager1;
            this.textEditMaterialExpDate.Name = "textEditMaterialExpDate";
            this.textEditMaterialExpDate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.textEditMaterialExpDate.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditMaterialExpDate.Properties.Appearance.Options.UseFont = true;
            this.textEditMaterialExpDate.Properties.Appearance.Options.UseForeColor = true;
            this.textEditMaterialExpDate.Properties.AutoHeight = false;
            this.textEditMaterialExpDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.textEditMaterialExpDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEditMaterialExpDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.textEditMaterialExpDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEditMaterialExpDate.Size = new System.Drawing.Size(525, 32);
            this.textEditMaterialExpDate.StyleController = this.layoutControl2;
            this.textEditMaterialExpDate.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "Root";
            columnDefinition36.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition36.Width = 50D;
            columnDefinition37.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition37.Width = 50D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition36,
            columnDefinition37});
            rowDefinition16.Height = 100D;
            rowDefinition16.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition16});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1201, 66);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.textEditMaterialLN;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(590, 46);
            this.layoutControlItem6.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem6.Text = "Lot,No:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(49, 16);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.textEditMaterialExpDate;
            this.layoutControlItem7.Location = new System.Drawing.Point(590, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem7.Size = new System.Drawing.Size(591, 46);
            this.layoutControlItem7.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem7.Text = "Exp.date";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(49, 16);
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.textEditGrossWeight);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl3.Location = new System.Drawing.Point(0, 337);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1182, 57, 650, 400);
            this.layoutControl3.Root = this.layoutControlGroup2;
            this.layoutControl3.Size = new System.Drawing.Size(1201, 63);
            this.layoutControl3.TabIndex = 3;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // textEditGrossWeight
            // 
            this.textEditGrossWeight.Location = new System.Drawing.Point(101, 17);
            this.textEditGrossWeight.MenuManager = this.toolbarFormManager1;
            this.textEditGrossWeight.Name = "textEditGrossWeight";
            this.textEditGrossWeight.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditGrossWeight.Properties.Appearance.Options.UseForeColor = true;
            this.textEditGrossWeight.Properties.AutoHeight = false;
            this.textEditGrossWeight.Size = new System.Drawing.Size(1083, 29);
            this.textEditGrossWeight.StyleController = this.layoutControl3;
            this.textEditGrossWeight.TabIndex = 4;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1201, 63);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.Control = this.textEditGrossWeight;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(1181, 43);
            this.layoutControlItem8.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem8.Text = "Gross Weight:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(81, 16);
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.textEditTare);
            this.layoutControl4.Controls.Add(this.textEditNetWeight);
            this.layoutControl4.Controls.Add(this.textEditWeighingBy);
            this.layoutControl4.Controls.Add(this.textEditWeighingDate);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl4.Location = new System.Drawing.Point(0, 400);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1182, 159, 650, 400);
            this.layoutControl4.Root = this.layoutControlGroup3;
            this.layoutControl4.Size = new System.Drawing.Size(1201, 107);
            this.layoutControl4.TabIndex = 4;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // textEditTare
            // 
            this.textEditTare.Location = new System.Drawing.Point(95, 17);
            this.textEditTare.MenuManager = this.toolbarFormManager1;
            this.textEditTare.Name = "textEditTare";
            this.textEditTare.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditTare.Properties.Appearance.Options.UseForeColor = true;
            this.textEditTare.Properties.AutoHeight = false;
            this.textEditTare.Size = new System.Drawing.Size(380, 29);
            this.textEditTare.StyleController = this.layoutControl4;
            this.textEditTare.TabIndex = 4;
            // 
            // textEditNetWeight
            // 
            this.textEditNetWeight.Location = new System.Drawing.Point(95, 60);
            this.textEditNetWeight.MenuManager = this.toolbarFormManager1;
            this.textEditNetWeight.Name = "textEditNetWeight";
            this.textEditNetWeight.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditNetWeight.Properties.Appearance.Options.UseForeColor = true;
            this.textEditNetWeight.Properties.AutoHeight = false;
            this.textEditNetWeight.Size = new System.Drawing.Size(380, 30);
            this.textEditNetWeight.StyleController = this.layoutControl4;
            this.textEditNetWeight.TabIndex = 5;
            // 
            // textEditWeighingBy
            // 
            this.textEditWeighingBy.Location = new System.Drawing.Point(567, 17);
            this.textEditWeighingBy.MenuManager = this.toolbarFormManager1;
            this.textEditWeighingBy.Name = "textEditWeighingBy";
            this.textEditWeighingBy.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditWeighingBy.Properties.Appearance.Options.UseForeColor = true;
            this.textEditWeighingBy.Properties.AutoHeight = false;
            this.textEditWeighingBy.Size = new System.Drawing.Size(617, 29);
            this.textEditWeighingBy.StyleController = this.layoutControl4;
            this.textEditWeighingBy.TabIndex = 6;
            // 
            // textEditWeighingDate
            // 
            this.textEditWeighingDate.Location = new System.Drawing.Point(567, 60);
            this.textEditWeighingDate.MenuManager = this.toolbarFormManager1;
            this.textEditWeighingDate.Name = "textEditWeighingDate";
            this.textEditWeighingDate.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditWeighingDate.Properties.Appearance.Options.UseForeColor = true;
            this.textEditWeighingDate.Properties.AutoHeight = false;
            this.textEditWeighingDate.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.textEditWeighingDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEditWeighingDate.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.textEditWeighingDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEditWeighingDate.Size = new System.Drawing.Size(617, 30);
            this.textEditWeighingDate.StyleController = this.layoutControl4;
            this.textEditWeighingDate.TabIndex = 7;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.layoutControlGroup3.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup3.Name = "Root";
            columnDefinition34.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition34.Width = 40D;
            columnDefinition35.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition35.Width = 60D;
            this.layoutControlGroup3.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition34,
            columnDefinition35});
            rowDefinition14.Height = 50D;
            rowDefinition14.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition15.Height = 50D;
            rowDefinition15.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup3.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition14,
            rowDefinition15});
            this.layoutControlGroup3.Size = new System.Drawing.Size(1201, 107);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.Control = this.textEditTare;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(472, 43);
            this.layoutControlItem9.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem9.Text = "Tare: ";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.Control = this.textEditNetWeight;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 43);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem10.Size = new System.Drawing.Size(472, 44);
            this.layoutControlItem10.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem10.Text = "Net Weight:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.Control = this.textEditWeighingBy;
            this.layoutControlItem11.Location = new System.Drawing.Point(472, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem11.Size = new System.Drawing.Size(709, 43);
            this.layoutControlItem11.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem11.Text = "Weighing by:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.Control = this.textEditWeighingDate;
            this.layoutControlItem12.Location = new System.Drawing.Point(472, 43);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem12.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem12.Size = new System.Drawing.Size(709, 44);
            this.layoutControlItem12.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem12.Text = "Date:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(75, 16);
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.textEditBalanceCode);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl5.Location = new System.Drawing.Point(0, 507);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1182, 214, 650, 400);
            this.layoutControl5.Root = this.layoutControlGroup4;
            this.layoutControl5.Size = new System.Drawing.Size(1201, 63);
            this.layoutControl5.TabIndex = 5;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // textEditBalanceCode
            // 
            this.textEditBalanceCode.Location = new System.Drawing.Point(102, 17);
            this.textEditBalanceCode.MenuManager = this.toolbarFormManager1;
            this.textEditBalanceCode.Name = "textEditBalanceCode";
            this.textEditBalanceCode.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.textEditBalanceCode.Properties.Appearance.Options.UseForeColor = true;
            this.textEditBalanceCode.Properties.AutoHeight = false;
            this.textEditBalanceCode.Size = new System.Drawing.Size(728, 29);
            this.textEditBalanceCode.StyleController = this.layoutControl5;
            this.textEditBalanceCode.TabIndex = 4;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13});
            this.layoutControlGroup4.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup4.Name = "Root";
            columnDefinition32.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition32.Width = 70D;
            columnDefinition33.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition33.Width = 30D;
            this.layoutControlGroup4.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition32,
            columnDefinition33});
            rowDefinition13.Height = 100D;
            rowDefinition13.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup4.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition13});
            this.layoutControlGroup4.Size = new System.Drawing.Size(1201, 63);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.textEditBalanceCode;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(827, 43);
            this.layoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem13.Text = "Balance Code:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(82, 16);
            // 
            // layoutControl6
            // 
            this.layoutControl6.Controls.Add(this.layoutControl7);
            this.layoutControl6.Controls.Add(this.simpleButton1);
            this.layoutControl6.Controls.Add(this.simpleButton2);
            this.layoutControl6.Controls.Add(this.simpleButton3);
            this.layoutControl6.Controls.Add(this.simpleButton4);
            this.layoutControl6.Controls.Add(this.simpleButton5);
            this.layoutControl6.Controls.Add(this.simpleButton6);
            this.layoutControl6.Controls.Add(this.simpleButton7);
            this.layoutControl6.Controls.Add(this.simpleButton8);
            this.layoutControl6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.layoutControl6.Location = new System.Drawing.Point(0, 609);
            this.layoutControl6.Name = "layoutControl6";
            this.layoutControl6.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1025, 229, 650, 400);
            this.layoutControl6.Root = this.layoutControlGroup5;
            this.layoutControl6.Size = new System.Drawing.Size(1134, 53);
            this.layoutControl6.TabIndex = 6;
            this.layoutControl6.Text = "layoutControl6";
            // 
            // layoutControl7
            // 
            this.layoutControl7.Location = new System.Drawing.Point(12, 12);
            this.layoutControl7.Name = "layoutControl7";
            this.layoutControl7.Root = null;
            this.layoutControl7.Size = new System.Drawing.Size(104, 32);
            this.layoutControl7.TabIndex = 12;
            this.layoutControl7.Text = "layoutControl7";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButton1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.Location = new System.Drawing.Point(17, 17);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(94, 22);
            this.simpleButton1.StyleController = this.layoutControl6;
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "Allow";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Location = new System.Drawing.Point(125, 17);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(96, 22);
            this.simpleButton2.StyleController = this.layoutControl6;
            this.simpleButton2.TabIndex = 5;
            this.simpleButton2.Text = "Tare";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Location = new System.Drawing.Point(235, 17);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(96, 22);
            this.simpleButton3.StyleController = this.layoutControl6;
            this.simpleButton3.TabIndex = 6;
            this.simpleButton3.Text = "N.W";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.Location = new System.Drawing.Point(345, 17);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(151, 22);
            this.simpleButton4.StyleController = this.layoutControl6;
            this.simpleButton4.TabIndex = 7;
            this.simpleButton4.Text = "List SQL";
            // 
            // simpleButton5
            // 
            this.simpleButton5.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButton5.Appearance.Options.UseBackColor = true;
            this.simpleButton5.Location = new System.Drawing.Point(510, 17);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(205, 22);
            this.simpleButton5.StyleController = this.layoutControl6;
            this.simpleButton5.TabIndex = 8;
            this.simpleButton5.Text = "Record List";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButton6.Appearance.Options.UseBackColor = true;
            this.simpleButton6.Location = new System.Drawing.Point(729, 17);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(151, 22);
            this.simpleButton6.StyleController = this.layoutControl6;
            this.simpleButton6.TabIndex = 9;
            this.simpleButton6.Text = "Continue";
            // 
            // simpleButton7
            // 
            this.simpleButton7.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButton7.Appearance.Options.UseBackColor = true;
            this.simpleButton7.Location = new System.Drawing.Point(894, 17);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(96, 22);
            this.simpleButton7.StyleController = this.layoutControl6;
            this.simpleButton7.TabIndex = 10;
            this.simpleButton7.Text = "Clear";
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButton8.Appearance.Options.UseBackColor = true;
            this.simpleButton8.Location = new System.Drawing.Point(1004, 17);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(96, 22);
            this.simpleButton8.StyleController = this.layoutControl6;
            this.simpleButton8.TabIndex = 11;
            this.simpleButton8.Text = "Print";
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22});
            this.layoutControlGroup5.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup5.Name = "Root";
            columnDefinition39.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition39.Width = 10D;
            columnDefinition40.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition40.Width = 10D;
            columnDefinition41.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition41.Width = 10D;
            columnDefinition42.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition42.Width = 15D;
            columnDefinition43.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition43.Width = 20D;
            columnDefinition44.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition44.Width = 15D;
            columnDefinition45.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition45.Width = 10D;
            columnDefinition46.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition46.Width = 10D;
            this.layoutControlGroup5.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition39,
            columnDefinition40,
            columnDefinition41,
            columnDefinition42,
            columnDefinition43,
            columnDefinition44,
            columnDefinition45,
            columnDefinition46});
            rowDefinition22.Height = 100D;
            rowDefinition22.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup5.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition22});
            this.layoutControlGroup5.Size = new System.Drawing.Size(1117, 56);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem14.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.Control = this.simpleButton1;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(108, 36);
            this.layoutControlItem14.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.Control = this.simpleButton2;
            this.layoutControlItem15.Location = new System.Drawing.Point(108, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem15.Size = new System.Drawing.Size(110, 36);
            this.layoutControlItem15.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.Control = this.simpleButton3;
            this.layoutControlItem16.Location = new System.Drawing.Point(218, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem16.Size = new System.Drawing.Size(110, 36);
            this.layoutControlItem16.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.Control = this.simpleButton4;
            this.layoutControlItem17.Location = new System.Drawing.Point(328, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem17.Size = new System.Drawing.Size(165, 36);
            this.layoutControlItem17.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.Control = this.simpleButton5;
            this.layoutControlItem18.Location = new System.Drawing.Point(493, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem18.Size = new System.Drawing.Size(219, 36);
            this.layoutControlItem18.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.Control = this.simpleButton6;
            this.layoutControlItem19.Location = new System.Drawing.Point(712, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem19.Size = new System.Drawing.Size(165, 36);
            this.layoutControlItem19.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.Control = this.simpleButton7;
            this.layoutControlItem20.Location = new System.Drawing.Point(877, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem20.Size = new System.Drawing.Size(110, 36);
            this.layoutControlItem20.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 10F);
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.Control = this.simpleButton8;
            this.layoutControlItem21.Location = new System.Drawing.Point(987, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.OptionsTableLayoutItem.ColumnIndex = 7;
            this.layoutControlItem21.Size = new System.Drawing.Size(110, 36);
            this.layoutControlItem21.Spacing = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.layoutControl7;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(108, 36);
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControl8
            // 
            this.layoutControl8.Controls.Add(this.simpleButtonAllow);
            this.layoutControl8.Controls.Add(this.simpleButtonTare);
            this.layoutControl8.Controls.Add(this.simpleButtonNW);
            this.layoutControl8.Controls.Add(this.simpleButtonListSQL);
            this.layoutControl8.Controls.Add(this.simpleButtonRecordList);
            this.layoutControl8.Controls.Add(this.simpleButtonContinue);
            this.layoutControl8.Controls.Add(this.simpleButtonClear);
            this.layoutControl8.Controls.Add(this.simpleButtonPrint);
            this.layoutControl8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.layoutControl8.Location = new System.Drawing.Point(0, 569);
            this.layoutControl8.Name = "layoutControl8";
            this.layoutControl8.Root = this.layoutControlGroup6;
            this.layoutControl8.Size = new System.Drawing.Size(1201, 60);
            this.layoutControl8.TabIndex = 10;
            this.layoutControl8.Text = "layoutControl8";
            // 
            // simpleButtonAllow
            // 
            this.simpleButtonAllow.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButtonAllow.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButtonAllow.Appearance.Options.UseBackColor = true;
            this.simpleButtonAllow.Appearance.Options.UseFont = true;
            this.simpleButtonAllow.Location = new System.Drawing.Point(12, 12);
            this.simpleButtonAllow.Name = "simpleButtonAllow";
            this.simpleButtonAllow.Size = new System.Drawing.Size(114, 22);
            this.simpleButtonAllow.StyleController = this.layoutControl8;
            this.simpleButtonAllow.TabIndex = 4;
            this.simpleButtonAllow.Text = "Allow";
            // 
            // simpleButtonTare
            // 
            this.simpleButtonTare.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButtonTare.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButtonTare.Appearance.Options.UseBackColor = true;
            this.simpleButtonTare.Appearance.Options.UseFont = true;
            this.simpleButtonTare.Location = new System.Drawing.Point(130, 12);
            this.simpleButtonTare.Name = "simpleButtonTare";
            this.simpleButtonTare.Size = new System.Drawing.Size(117, 22);
            this.simpleButtonTare.StyleController = this.layoutControl8;
            this.simpleButtonTare.TabIndex = 5;
            this.simpleButtonTare.Text = "Tare";
            // 
            // simpleButtonNW
            // 
            this.simpleButtonNW.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButtonNW.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButtonNW.Appearance.Options.UseBackColor = true;
            this.simpleButtonNW.Appearance.Options.UseFont = true;
            this.simpleButtonNW.Location = new System.Drawing.Point(251, 12);
            this.simpleButtonNW.Name = "simpleButtonNW";
            this.simpleButtonNW.Size = new System.Drawing.Size(117, 22);
            this.simpleButtonNW.StyleController = this.layoutControl8;
            this.simpleButtonNW.TabIndex = 6;
            this.simpleButtonNW.Text = "N.W";
            // 
            // simpleButtonListSQL
            // 
            this.simpleButtonListSQL.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButtonListSQL.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButtonListSQL.Appearance.Options.UseBackColor = true;
            this.simpleButtonListSQL.Appearance.Options.UseFont = true;
            this.simpleButtonListSQL.Location = new System.Drawing.Point(372, 12);
            this.simpleButtonListSQL.Name = "simpleButtonListSQL";
            this.simpleButtonListSQL.Size = new System.Drawing.Size(189, 22);
            this.simpleButtonListSQL.StyleController = this.layoutControl8;
            this.simpleButtonListSQL.TabIndex = 7;
            this.simpleButtonListSQL.Text = "List SQL";
            // 
            // simpleButtonRecordList
            // 
            this.simpleButtonRecordList.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButtonRecordList.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButtonRecordList.Appearance.Options.UseBackColor = true;
            this.simpleButtonRecordList.Appearance.Options.UseFont = true;
            this.simpleButtonRecordList.Location = new System.Drawing.Point(565, 12);
            this.simpleButtonRecordList.Name = "simpleButtonRecordList";
            this.simpleButtonRecordList.Size = new System.Drawing.Size(189, 22);
            this.simpleButtonRecordList.StyleController = this.layoutControl8;
            this.simpleButtonRecordList.TabIndex = 8;
            this.simpleButtonRecordList.Text = "Record List";
            // 
            // simpleButtonContinue
            // 
            this.simpleButtonContinue.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButtonContinue.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButtonContinue.Appearance.Options.UseBackColor = true;
            this.simpleButtonContinue.Appearance.Options.UseFont = true;
            this.simpleButtonContinue.Location = new System.Drawing.Point(758, 12);
            this.simpleButtonContinue.Name = "simpleButtonContinue";
            this.simpleButtonContinue.Size = new System.Drawing.Size(189, 22);
            this.simpleButtonContinue.StyleController = this.layoutControl8;
            this.simpleButtonContinue.TabIndex = 9;
            this.simpleButtonContinue.Text = "Continue";
            // 
            // simpleButtonClear
            // 
            this.simpleButtonClear.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButtonClear.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButtonClear.Appearance.Options.UseBackColor = true;
            this.simpleButtonClear.Appearance.Options.UseFont = true;
            this.simpleButtonClear.Location = new System.Drawing.Point(951, 12);
            this.simpleButtonClear.Name = "simpleButtonClear";
            this.simpleButtonClear.Size = new System.Drawing.Size(117, 22);
            this.simpleButtonClear.StyleController = this.layoutControl8;
            this.simpleButtonClear.TabIndex = 10;
            this.simpleButtonClear.Text = "Clear";
            // 
            // simpleButtonPrint
            // 
            this.simpleButtonPrint.Appearance.BackColor = System.Drawing.Color.Blue;
            this.simpleButtonPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.simpleButtonPrint.Appearance.Options.UseBackColor = true;
            this.simpleButtonPrint.Appearance.Options.UseFont = true;
            this.simpleButtonPrint.Enabled = false;
            this.simpleButtonPrint.Location = new System.Drawing.Point(1072, 12);
            this.simpleButtonPrint.Name = "simpleButtonPrint";
            this.simpleButtonPrint.Size = new System.Drawing.Size(117, 22);
            this.simpleButtonPrint.StyleController = this.layoutControl8;
            this.simpleButtonPrint.TabIndex = 11;
            this.simpleButtonPrint.Text = "Print";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30});
            this.layoutControlGroup6.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            columnDefinition24.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition24.Width = 10.204081632653061D;
            columnDefinition25.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition25.Width = 10.204081632653061D;
            columnDefinition26.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition26.Width = 10.204081632653061D;
            columnDefinition27.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition27.Width = 16.3265306122449D;
            columnDefinition28.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition28.Width = 16.3265306122449D;
            columnDefinition29.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition29.Width = 16.3265306122449D;
            columnDefinition30.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition30.Width = 10.204081632653061D;
            columnDefinition31.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition31.Width = 10.204081632653061D;
            this.layoutControlGroup6.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition24,
            columnDefinition25,
            columnDefinition26,
            columnDefinition27,
            columnDefinition28,
            columnDefinition29,
            columnDefinition30,
            columnDefinition31});
            rowDefinition12.Height = 100D;
            rowDefinition12.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup6.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition12});
            this.layoutControlGroup6.Size = new System.Drawing.Size(1201, 60);
            this.layoutControlGroup6.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.simpleButtonAllow;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(118, 40);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.simpleButtonTare;
            this.layoutControlItem24.Location = new System.Drawing.Point(118, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem24.Size = new System.Drawing.Size(121, 40);
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.simpleButtonNW;
            this.layoutControlItem25.Location = new System.Drawing.Point(239, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem25.Size = new System.Drawing.Size(121, 40);
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.simpleButtonListSQL;
            this.layoutControlItem26.Location = new System.Drawing.Point(360, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem26.Size = new System.Drawing.Size(193, 40);
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.simpleButtonRecordList;
            this.layoutControlItem27.Location = new System.Drawing.Point(553, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem27.Size = new System.Drawing.Size(193, 40);
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.simpleButtonContinue;
            this.layoutControlItem28.Location = new System.Drawing.Point(746, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem28.Size = new System.Drawing.Size(193, 40);
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.simpleButtonClear;
            this.layoutControlItem29.Location = new System.Drawing.Point(939, 0);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem29.Size = new System.Drawing.Size(121, 40);
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.simpleButtonPrint;
            this.layoutControlItem30.Location = new System.Drawing.Point(1060, 0);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.OptionsTableLayoutItem.ColumnIndex = 7;
            this.layoutControlItem30.Size = new System.Drawing.Size(121, 40);
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextVisible = false;
            // 
            // MainForm
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1201, 629);
            this.Controls.Add(this.layoutControl8);
            this.Controls.Add(this.layoutControl5);
            this.Controls.Add(this.layoutControl4);
            this.Controls.Add(this.layoutControl3);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.toolbarFormControl1);
            this.Font = new System.Drawing.Font("Tahoma", 8.5F);
            this.IconOptions.Image = global::WeighingApp.Properties.Resources.favicon;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ToolbarFormControl = this.toolbarFormControl1;
            ((System.ComponentModel.ISupportInitialize)(this.toolbarFormControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolbarFormManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductExpDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMaterialName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMaterialCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditProductLN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditMaterialLN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMaterialExpDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditGrossWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditTare.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNetWeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWeighingBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditWeighingDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEditBalanceCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).EndInit();
            this.layoutControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl8)).EndInit();
            this.layoutControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.ToolbarForm.ToolbarFormControl toolbarFormControl1;
        private DevExpress.XtraBars.ToolbarForm.ToolbarFormManager toolbarFormManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraLayout.LayoutControl layoutControl6;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraEditors.TextEdit textEditBalanceCode;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraEditors.TextEdit textEditTare;
        private DevExpress.XtraEditors.TextEdit textEditNetWeight;
        private DevExpress.XtraEditors.TextEdit textEditWeighingBy;
        private DevExpress.XtraEditors.TextEdit textEditWeighingDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraEditors.TextEdit textEditGrossWeight;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.TextEdit textEditMaterialLN;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEditProductName;
        private DevExpress.XtraEditors.TextEdit textEditProductExpDate;
        private DevExpress.XtraEditors.TextEdit textEditMaterialName;
        private DevExpress.XtraEditors.TextEdit textEditMaterialCode;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUpdate;
        private DevExpress.XtraLayout.LayoutControl layoutControl7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControl layoutControl8;
        private DevExpress.XtraEditors.SimpleButton simpleButtonAllow;
        private DevExpress.XtraEditors.SimpleButton simpleButtonTare;
        private DevExpress.XtraEditors.SimpleButton simpleButtonNW;
        private DevExpress.XtraEditors.SimpleButton simpleButtonListSQL;
        private DevExpress.XtraEditors.SimpleButton simpleButtonRecordList;
        private DevExpress.XtraEditors.SimpleButton simpleButtonContinue;
        private DevExpress.XtraEditors.SimpleButton simpleButtonClear;
        private DevExpress.XtraEditors.SimpleButton simpleButtonPrint;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraEditors.TextEdit textEditMaterialExpDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItemUpdateFromSQL;
        private DevExpress.XtraBars.BarButtonItem barButtonItemImportData;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarHeaderItem barHeaderItemServerConnectionStatus;
        private DevExpress.XtraEditors.TextEdit textEditProductLN;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}