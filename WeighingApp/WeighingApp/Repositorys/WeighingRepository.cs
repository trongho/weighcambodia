﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WeighingApp.Helpers;
using WeighingApp.Models;

namespace WeighingApp.Repositorys
{
    class WeighingRepository
    {
        public WeighingRepository()
        {
        }

        public async Task<List<Weighing>> GetAll()
        {
            string URI = Constant.BaseURL + "weighing";
            List<Weighing> entrys = new List<Weighing>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Weighing[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys;
        }

        public async Task<Weighing> GetUnderID(String id)
        {
            string URI = Constant.BaseURL + "weighing/" + id;
            List<Weighing> entrys = new List<Weighing>();
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            entrys = JsonConvert.DeserializeObject<Weighing[]>(fileJsonString).ToList();
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return entrys[0];
        }

        public async Task<long> Create(Weighing entry)
        {
            long result = -1;
            string URI = Constant.BaseURL + "weighing/Post";

            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(entry), Encoding.UTF8, "application/json");
                try
                {
                    using (var response = await client.PostAsync(URI, content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = 1;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return result;
        }

        public async Task<long> Delete()
        {
            long result = -1;
            string URI = Constant.BaseURL + "weighing/Delete";
            using (var client = new HttpClient())
            {
                using (var response = await client.DeleteAsync(URI))
                {


                    if (response.IsSuccessStatusCode)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public async Task<String> GetID()
        {
            string URI = Constant.BaseURL + "weighing/GetID";
            String id = "";
            using (var client = new HttpClient())
            {
                try
                {
                    using (var response = await client.GetAsync(URI))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var fileJsonString = await response.Content.ReadAsStringAsync();
                            id = fileJsonString;
                        }
                    }
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine(e);
                }
            }
            return id;
        }
    }
}
