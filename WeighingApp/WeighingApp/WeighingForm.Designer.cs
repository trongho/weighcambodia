﻿
namespace WeighingApp
{
    partial class WeighingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition6 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition7 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition8 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition9 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition10 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition11 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition12 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition5 = new DevExpress.XtraLayout.RowDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeighingForm));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.label1 = new System.Windows.Forms.Label();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.label2 = new System.Windows.Forms.Label();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.label3 = new System.Windows.Forms.Label();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditTareScaleA = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.textEditNetWeightScaleA = new DevExpress.XtraEditors.TextEdit();
            this.textEditTareScaleB = new DevExpress.XtraEditors.TextEdit();
            this.textEditNetWeightScaleB = new DevExpress.XtraEditors.TextEdit();
            this.textEditTareScaleC = new DevExpress.XtraEditors.TextEdit();
            this.textEditNetWeightScaleC = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonTareA = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButtonTareB = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonTareC = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButtonPut = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTareScaleA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNetWeightScaleA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTareScaleB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNetWeightScaleB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTareScaleC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNetWeightScaleC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.label3);
            this.layoutControl1.Controls.Add(this.label2);
            this.layoutControl1.Controls.Add(this.label1);
            this.layoutControl1.Controls.Add(this.pictureBox3);
            this.layoutControl1.Controls.Add(this.pictureBox2);
            this.layoutControl1.Controls.Add(this.pictureBox1);
            this.layoutControl1.Controls.Add(this.textEditTareScaleA);
            this.layoutControl1.Controls.Add(this.textEditNetWeightScaleA);
            this.layoutControl1.Controls.Add(this.textEditTareScaleB);
            this.layoutControl1.Controls.Add(this.textEditNetWeightScaleB);
            this.layoutControl1.Controls.Add(this.textEditTareScaleC);
            this.layoutControl1.Controls.Add(this.textEditNetWeightScaleC);
            this.layoutControl1.Controls.Add(this.simpleButtonTareA);
            this.layoutControl1.Controls.Add(this.simpleButtonTareB);
            this.layoutControl1.Controls.Add(this.simpleButtonTareC);
            this.layoutControl1.Controls.Add(this.simpleButtonPut);
            this.layoutControl1.Controls.Add(this.simpleButton2);
            this.layoutControl1.Controls.Add(this.simpleButton3);
            this.layoutControl1.Controls.Add(this.simpleButton4);
            this.layoutControl1.Controls.Add(this.simpleButton5);
            this.layoutControl1.Controls.Add(this.simpleButton6);
            this.layoutControl1.Controls.Add(this.simpleButton7);
            this.layoutControl1.Controls.Add(this.simpleButton8);
            this.layoutControl1.Controls.Add(this.simpleButton9);
            this.layoutControl1.Controls.Add(this.simpleButton10);
            this.layoutControl1.Controls.Add(this.simpleButton11);
            this.layoutControl1.Controls.Add(this.simpleButton12);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(949, 86, 650, 400);
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(937, 360);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem13,
            this.layoutControlItem12,
            this.layoutControlItem11,
            this.layoutControlItem10,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItem15,
            this.layoutControlItem14,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27});
            this.Root.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.Root.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 8.3333333333333339D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 8.3333333333333339D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 8.3333333333333339D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition4.Width = 8.3333333333333339D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 8.3333333333333339D;
            columnDefinition6.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition6.Width = 8.3333333333333339D;
            columnDefinition7.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition7.Width = 8.3333333333333339D;
            columnDefinition8.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition8.Width = 8.3333333333333339D;
            columnDefinition9.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition9.Width = 8.3333333333333339D;
            columnDefinition10.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition10.Width = 8.3333333333333339D;
            columnDefinition11.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition11.Width = 8.3333333333333339D;
            columnDefinition12.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition12.Width = 8.3333333333333339D;
            this.Root.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5,
            columnDefinition6,
            columnDefinition7,
            columnDefinition8,
            columnDefinition9,
            columnDefinition10,
            columnDefinition11,
            columnDefinition12});
            rowDefinition1.Height = 20D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 50D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition3.Height = 10D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition4.Height = 10D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition5.Height = 10D;
            rowDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            this.Root.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4,
            rowDefinition5});
            this.Root.Size = new System.Drawing.Size(937, 360);
            this.Root.TextVisible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 80);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(300, 166);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.pictureBox1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItem1.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem1.Size = new System.Drawing.Size(304, 170);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(316, 80);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(300, 166);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.pictureBox2;
            this.layoutControlItem2.Location = new System.Drawing.Point(304, 68);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem2.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItem2.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem2.Size = new System.Drawing.Size(304, 170);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(620, 80);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(305, 166);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.pictureBox3;
            this.layoutControlItem3.Location = new System.Drawing.Point(608, 68);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnIndex = 8;
            this.layoutControlItem3.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItem3.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlItem3.Size = new System.Drawing.Size(309, 170);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(300, 64);
            this.label1.TabIndex = 7;
            this.label1.Text = "SCALE 1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.label1;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItem4.Size = new System.Drawing.Size(304, 68);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.Location = new System.Drawing.Point(316, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(300, 64);
            this.label2.TabIndex = 8;
            this.label2.Text = "SCALE 2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.label2;
            this.layoutControlItem5.Location = new System.Drawing.Point(304, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem5.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItem5.Size = new System.Drawing.Size(304, 68);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(620, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(305, 64);
            this.label3.TabIndex = 9;
            this.label3.Text = "SCALE 3";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.label3;
            this.layoutControlItem6.Location = new System.Drawing.Point(608, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnIndex = 8;
            this.layoutControlItem6.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItem6.Size = new System.Drawing.Size(309, 68);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // textEditTareScaleA
            // 
            this.textEditTareScaleA.Location = new System.Drawing.Point(12, 250);
            this.textEditTareScaleA.Name = "textEditTareScaleA";
            this.textEditTareScaleA.Size = new System.Drawing.Size(148, 20);
            this.textEditTareScaleA.StyleController = this.layoutControl1;
            this.textEditTareScaleA.TabIndex = 10;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEditTareScaleA;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 238);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem7.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem7.Size = new System.Drawing.Size(152, 34);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // textEditNetWeightScaleA
            // 
            this.textEditNetWeightScaleA.Location = new System.Drawing.Point(164, 250);
            this.textEditNetWeightScaleA.Name = "textEditNetWeightScaleA";
            this.textEditNetWeightScaleA.Size = new System.Drawing.Size(148, 20);
            this.textEditNetWeightScaleA.StyleController = this.layoutControl1;
            this.textEditNetWeightScaleA.TabIndex = 11;
            // 
            // textEditTareScaleB
            // 
            this.textEditTareScaleB.Location = new System.Drawing.Point(316, 250);
            this.textEditTareScaleB.Name = "textEditTareScaleB";
            this.textEditTareScaleB.Size = new System.Drawing.Size(148, 20);
            this.textEditTareScaleB.StyleController = this.layoutControl1;
            this.textEditTareScaleB.TabIndex = 12;
            // 
            // textEditNetWeightScaleB
            // 
            this.textEditNetWeightScaleB.Location = new System.Drawing.Point(468, 250);
            this.textEditNetWeightScaleB.Name = "textEditNetWeightScaleB";
            this.textEditNetWeightScaleB.Size = new System.Drawing.Size(148, 20);
            this.textEditNetWeightScaleB.StyleController = this.layoutControl1;
            this.textEditNetWeightScaleB.TabIndex = 13;
            // 
            // textEditTareScaleC
            // 
            this.textEditTareScaleC.Location = new System.Drawing.Point(620, 250);
            this.textEditTareScaleC.Name = "textEditTareScaleC";
            this.textEditTareScaleC.Size = new System.Drawing.Size(148, 20);
            this.textEditTareScaleC.StyleController = this.layoutControl1;
            this.textEditTareScaleC.TabIndex = 14;
            // 
            // textEditNetWeightScaleC
            // 
            this.textEditNetWeightScaleC.Location = new System.Drawing.Point(772, 250);
            this.textEditNetWeightScaleC.Name = "textEditNetWeightScaleC";
            this.textEditNetWeightScaleC.Size = new System.Drawing.Size(153, 20);
            this.textEditNetWeightScaleC.StyleController = this.layoutControl1;
            this.textEditNetWeightScaleC.TabIndex = 15;
            // 
            // simpleButtonTareA
            // 
            this.simpleButtonTareA.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            this.simpleButtonTareA.Appearance.Options.UseBackColor = true;
            this.simpleButtonTareA.Location = new System.Drawing.Point(12, 318);
            this.simpleButtonTareA.Name = "simpleButtonTareA";
            this.simpleButtonTareA.Size = new System.Drawing.Size(300, 22);
            this.simpleButtonTareA.StyleController = this.layoutControl1;
            this.simpleButtonTareA.TabIndex = 16;
            this.simpleButtonTareA.Text = "TARE";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.simpleButtonTareA;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 306);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItem13.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem13.Size = new System.Drawing.Size(304, 34);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // simpleButtonTareB
            // 
            this.simpleButtonTareB.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            this.simpleButtonTareB.Appearance.Options.UseBackColor = true;
            this.simpleButtonTareB.Location = new System.Drawing.Point(316, 318);
            this.simpleButtonTareB.Name = "simpleButtonTareB";
            this.simpleButtonTareB.Size = new System.Drawing.Size(300, 22);
            this.simpleButtonTareB.StyleController = this.layoutControl1;
            this.simpleButtonTareB.TabIndex = 17;
            this.simpleButtonTareB.Text = "TARE";
            // 
            // simpleButtonTareC
            // 
            this.simpleButtonTareC.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            this.simpleButtonTareC.Appearance.Options.UseBackColor = true;
            this.simpleButtonTareC.Location = new System.Drawing.Point(620, 318);
            this.simpleButtonTareC.Name = "simpleButtonTareC";
            this.simpleButtonTareC.Size = new System.Drawing.Size(305, 22);
            this.simpleButtonTareC.StyleController = this.layoutControl1;
            this.simpleButtonTareC.TabIndex = 18;
            this.simpleButtonTareC.Text = "TARE";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEditNetWeightScaleC;
            this.layoutControlItem12.Location = new System.Drawing.Point(760, 238);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.OptionsTableLayoutItem.ColumnIndex = 10;
            this.layoutControlItem12.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem12.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem12.Size = new System.Drawing.Size(157, 34);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.textEditTareScaleC;
            this.layoutControlItem11.Location = new System.Drawing.Point(608, 238);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.OptionsTableLayoutItem.ColumnIndex = 8;
            this.layoutControlItem11.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem11.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem11.Size = new System.Drawing.Size(152, 34);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.textEditNetWeightScaleB;
            this.layoutControlItem10.Location = new System.Drawing.Point(456, 238);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem10.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem10.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem10.Size = new System.Drawing.Size(152, 34);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEditTareScaleB;
            this.layoutControlItem9.Location = new System.Drawing.Point(304, 238);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem9.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem9.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem9.Size = new System.Drawing.Size(152, 34);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEditNetWeightScaleA;
            this.layoutControlItem8.Location = new System.Drawing.Point(152, 238);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem8.OptionsTableLayoutItem.ColumnSpan = 2;
            this.layoutControlItem8.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlItem8.Size = new System.Drawing.Size(152, 34);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.simpleButtonTareC;
            this.layoutControlItem15.Location = new System.Drawing.Point(608, 306);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.OptionsTableLayoutItem.ColumnIndex = 8;
            this.layoutControlItem15.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItem15.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem15.Size = new System.Drawing.Size(309, 34);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.simpleButtonTareB;
            this.layoutControlItem14.Location = new System.Drawing.Point(304, 306);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem14.OptionsTableLayoutItem.ColumnSpan = 4;
            this.layoutControlItem14.OptionsTableLayoutItem.RowIndex = 4;
            this.layoutControlItem14.Size = new System.Drawing.Size(304, 34);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // simpleButtonPut
            // 
            this.simpleButtonPut.Location = new System.Drawing.Point(12, 284);
            this.simpleButtonPut.Name = "simpleButtonPut";
            this.simpleButtonPut.Size = new System.Drawing.Size(72, 22);
            this.simpleButtonPut.StyleController = this.layoutControl1;
            this.simpleButtonPut.TabIndex = 19;
            this.simpleButtonPut.Text = "PUT";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(88, 284);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(72, 22);
            this.simpleButton2.StyleController = this.layoutControl1;
            this.simpleButton2.TabIndex = 20;
            this.simpleButton2.Text = "REMOVE";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.simpleButton2;
            this.layoutControlItem17.Location = new System.Drawing.Point(76, 272);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlItem17.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem17.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem17.Text = "REMOVE";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.simpleButtonPut;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 272);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem16.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(164, 284);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(72, 22);
            this.simpleButton3.StyleController = this.layoutControl1;
            this.simpleButton3.TabIndex = 21;
            this.simpleButton3.Text = "PUT";
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.simpleButton3;
            this.layoutControlItem18.Location = new System.Drawing.Point(152, 272);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.OptionsTableLayoutItem.ColumnIndex = 2;
            this.layoutControlItem18.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem18.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(240, 284);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(72, 22);
            this.simpleButton4.StyleController = this.layoutControl1;
            this.simpleButton4.TabIndex = 22;
            this.simpleButton4.Text = "REMOVE";
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.simpleButton4;
            this.layoutControlItem19.Location = new System.Drawing.Point(228, 272);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.OptionsTableLayoutItem.ColumnIndex = 3;
            this.layoutControlItem19.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem19.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(316, 284);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(72, 22);
            this.simpleButton5.StyleController = this.layoutControl1;
            this.simpleButton5.TabIndex = 23;
            this.simpleButton5.Text = "PUT";
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.simpleButton5;
            this.layoutControlItem20.Location = new System.Drawing.Point(304, 272);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.OptionsTableLayoutItem.ColumnIndex = 4;
            this.layoutControlItem20.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem20.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // simpleButton6
            // 
            this.simpleButton6.Location = new System.Drawing.Point(392, 284);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(72, 22);
            this.simpleButton6.StyleController = this.layoutControl1;
            this.simpleButton6.TabIndex = 24;
            this.simpleButton6.Text = "REMOVE";
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.simpleButton6;
            this.layoutControlItem21.Location = new System.Drawing.Point(380, 272);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.OptionsTableLayoutItem.ColumnIndex = 5;
            this.layoutControlItem21.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem21.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextVisible = false;
            // 
            // simpleButton7
            // 
            this.simpleButton7.Location = new System.Drawing.Point(468, 284);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(72, 22);
            this.simpleButton7.StyleController = this.layoutControl1;
            this.simpleButton7.TabIndex = 25;
            this.simpleButton7.Text = "PUT";
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.simpleButton7;
            this.layoutControlItem22.Location = new System.Drawing.Point(456, 272);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.OptionsTableLayoutItem.ColumnIndex = 6;
            this.layoutControlItem22.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem22.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextVisible = false;
            // 
            // simpleButton8
            // 
            this.simpleButton8.Location = new System.Drawing.Point(544, 284);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(72, 22);
            this.simpleButton8.StyleController = this.layoutControl1;
            this.simpleButton8.TabIndex = 26;
            this.simpleButton8.Text = "REMOVE";
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.simpleButton8;
            this.layoutControlItem23.Location = new System.Drawing.Point(532, 272);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.OptionsTableLayoutItem.ColumnIndex = 7;
            this.layoutControlItem23.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem23.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextVisible = false;
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(620, 284);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(72, 22);
            this.simpleButton9.StyleController = this.layoutControl1;
            this.simpleButton9.TabIndex = 27;
            this.simpleButton9.Text = "PUT";
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.simpleButton9;
            this.layoutControlItem24.Location = new System.Drawing.Point(608, 272);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.OptionsTableLayoutItem.ColumnIndex = 8;
            this.layoutControlItem24.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem24.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // simpleButton10
            // 
            this.simpleButton10.Location = new System.Drawing.Point(696, 284);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(72, 22);
            this.simpleButton10.StyleController = this.layoutControl1;
            this.simpleButton10.TabIndex = 28;
            this.simpleButton10.Text = "REMOVE";
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.simpleButton10;
            this.layoutControlItem25.Location = new System.Drawing.Point(684, 272);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.OptionsTableLayoutItem.ColumnIndex = 9;
            this.layoutControlItem25.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem25.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // simpleButton11
            // 
            this.simpleButton11.Location = new System.Drawing.Point(772, 284);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(72, 22);
            this.simpleButton11.StyleController = this.layoutControl1;
            this.simpleButton11.TabIndex = 29;
            this.simpleButton11.Text = "PUT";
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.simpleButton11;
            this.layoutControlItem26.Location = new System.Drawing.Point(760, 272);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.OptionsTableLayoutItem.ColumnIndex = 10;
            this.layoutControlItem26.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem26.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // simpleButton12
            // 
            this.simpleButton12.Location = new System.Drawing.Point(848, 284);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(77, 22);
            this.simpleButton12.StyleController = this.layoutControl1;
            this.simpleButton12.TabIndex = 30;
            this.simpleButton12.Text = "REMOVE";
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.simpleButton12;
            this.layoutControlItem27.Location = new System.Drawing.Point(836, 272);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.OptionsTableLayoutItem.ColumnIndex = 11;
            this.layoutControlItem27.OptionsTableLayoutItem.RowIndex = 3;
            this.layoutControlItem27.Size = new System.Drawing.Size(81, 34);
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // WeighingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 360);
            this.Controls.Add(this.layoutControl1);
            this.Name = "WeighingForm";
            this.Text = "WeighingForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTareScaleA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNetWeightScaleA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTareScaleB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNetWeightScaleB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditTareScaleC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditNetWeightScaleC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.TextEdit textEditTareScaleA;
        private DevExpress.XtraEditors.TextEdit textEditNetWeightScaleA;
        private DevExpress.XtraEditors.TextEdit textEditTareScaleB;
        private DevExpress.XtraEditors.TextEdit textEditNetWeightScaleB;
        private DevExpress.XtraEditors.TextEdit textEditTareScaleC;
        private DevExpress.XtraEditors.TextEdit textEditNetWeightScaleC;
        private DevExpress.XtraEditors.SimpleButton simpleButtonTareA;
        private DevExpress.XtraEditors.SimpleButton simpleButtonTareB;
        private DevExpress.XtraEditors.SimpleButton simpleButtonTareC;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraEditors.SimpleButton simpleButtonPut;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
    }
}