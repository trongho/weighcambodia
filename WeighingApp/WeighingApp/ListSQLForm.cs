﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeighingApp.Models;
using WeighingApp.Repositorys;

namespace WeighingApp
{
    public partial class ListSQLForm : Form
    {
        internal MainForm m_MainForm = null;
        ProductRepository productRepository = null;
        MaterialRepository materialRepository = null;
        public ListSQLForm(MainForm mainForm)
        {
            InitializeComponent();
            m_MainForm = mainForm;

            productRepository = new ProductRepository();
            materialRepository = new MaterialRepository();
            gridViewProduct.ClearSorting();
            gridViewMaterial.ClearSorting();
            loadGridViewAsync();
        }

        public async Task loadGridViewAsync()
        {
            if (m_MainForm.updateFromExcelClick ==true)
            {
                List<Product> products = m_MainForm.productsPublic;
                gridControlProduct.DataSource = products;
                List<Material> materials = m_MainForm.materialsPublic;
                gridControlMaterial.DataSource = materials;
            }
            else
            {
                List<Product> products = await productRepository.GetAll();
                gridControlProduct.DataSource = products;
                List<Material> materials = await materialRepository.GetAll();
                gridControlMaterial.DataSource = materials;
            }
        }
    }
}
