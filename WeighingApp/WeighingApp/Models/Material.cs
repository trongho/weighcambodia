﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeighingApp.Models
{
    class Material
    {
      public string LotNo{get;set;}
      public string MaterialName{get;set;}
      public string Code{get;set;}
      public DateTime? ExpDate{get;set;}
    }
}
