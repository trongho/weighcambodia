﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeighingApp.Models
{
    class Weighing
    {
      public string WeighingID{get;set;}
      public string ProductLN{get;set;}
      public DateTime? ProductExpDate {get;set;}
      public string ProductName{get;set;}
      public string MaterialLN{get;set;}
      public DateTime? MaterialExpDate{get;set;}
      public string MaterialCode{get;set;}
      public string MaterialName{get;set;}
      public decimal GrossWeight{get;set;}
      public decimal Tare{get;set;}
      public decimal NetWeight{get;set;}
      public string WeighingBy{get;set;}
      public string BalanceCode{get;set;}
      public DateTime? WeighingDate {get;set;}
    }
}
