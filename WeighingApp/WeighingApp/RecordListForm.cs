﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeighingApp.Models;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace WeighingApp
{
    public partial class RecordListForm : Form
    {
        internal MainForm m_MainForm = null;
        public RecordListForm(MainForm mainForm)
        {
            InitializeComponent();
            m_MainForm = mainForm;
            gridView1.ClearSorting();
            loadGridView();

            
            if (gridView1.RowCount==0)
            {
                simpleButtonExportExcel.Enabled=false;
            }
        }

        public void loadGridView()
        {
                List<Weighing> weighings = m_MainForm.weighingsPublic;
                gridControl1.DataSource = weighings;
        }

        private void simpleButtonExportExcel_Click(object sender, EventArgs e)
        {
            List<Weighing> weighings = m_MainForm.weighingsPublic;
            FileInfo file = new FileInfo("d:\\weighing-export--"+weighings[0].WeighingID+"-"+weighings[weighings.Count-1].WeighingID+"--"+DateTime.Now.ToString("yyyy-MM-dd") +".xlsx");
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("test");
                worksheet.Cells.Style.Font.SetFromFont(new Font("Calibri", 10));
                worksheet.Cells.AutoFitColumns();

                worksheet.Columns.Width=28;
               

                worksheet.Row(2).Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Row(2).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#FF0000"));

                worksheet.Column(5).Style.Numberformat.Format = "yyyy-MM-dd";
                worksheet.Column(9).Style.Numberformat.Format = "yyyy-MM-dd";
                worksheet.Column(14).Style.Numberformat.Format = "yyyy-MM-dd";

                worksheet.Cells[2, 2].Value ="Weighing ID";
                worksheet.Cells[2, 3].Value = "Product Lot No";
                worksheet.Cells[2, 4].Value = "Product Name";
                worksheet.Cells[2, 5].Value = "Product Exp.Date";
                worksheet.Cells[2, 6].Value = "Material Lot No";
                worksheet.Cells[2, 7].Value = "Material Code";
                worksheet.Cells[2, 8].Value = "Material Name";
                worksheet.Cells[2, 9].Value = "Material Exp.Date";
                worksheet.Cells[2, 10].Value = "Tare";
                worksheet.Cells[2, 11].Value = "Net Weight";
                worksheet.Cells[2, 12].Value = "Gross Weight";
                worksheet.Cells[2, 13].Value = "Weigh By";
                worksheet.Cells[2, 14].Value = "Weighing Date ";
                worksheet.Cells[2, 15].Value = "Balance Code";

                int totalRows = weighings.Count;
                int i = 0;
                for (int row = 3; row <= totalRows +2; row++)
                {
                    worksheet.Cells[row, 2].Value = weighings[i].WeighingID;
                    worksheet.Cells[row, 3].Value = weighings[i].ProductLN;
                    worksheet.Cells[row, 4].Value = weighings[i].ProductName;
                    worksheet.Cells[row, 5].Value = weighings[i].ProductExpDate;
                    worksheet.Cells[row, 6].Value = weighings[i].MaterialLN;
                    worksheet.Cells[row, 7].Value = weighings[i].MaterialCode;
                    worksheet.Cells[row, 8].Value = weighings[i].MaterialName;
                    worksheet.Cells[row, 9].Value = weighings[i].MaterialExpDate;
                    worksheet.Cells[row, 10].Value = weighings[i].Tare;
                    worksheet.Cells[row, 11].Value = weighings[i].NetWeight;
                    worksheet.Cells[row, 12].Value = weighings[i].GrossWeight;
                    worksheet.Cells[row, 13].Value = weighings[i].WeighingBy;
                    worksheet.Cells[row, 14].Value = weighings[i].WeighingDate;
                    worksheet.Cells[row, 15].Value = weighings[i].BalanceCode;
                    i++;
                }
                package.Save();

            }

            MessageBox.Show("D:\\weighing-export--"+weighings[0].WeighingID+"-"+weighings[weighings.Count-1].WeighingID+"--"+DateTime.Now.ToString("yyyy - MM - dd") +".xlsx");
        }
    }
}
