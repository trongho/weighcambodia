﻿
namespace WeighingApp
{
    partial class RecordListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWeighingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductLN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductExpDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProductName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialLN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialExpDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrossWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetWeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeighingBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBalanceCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeighingDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleButtonExportExcel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1047, 467);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(12, 12);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1023, 443);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWeighingID,
            this.colProductLN,
            this.colProductExpDate,
            this.colProductName,
            this.colMaterialLN,
            this.colMaterialExpDate,
            this.colMaterialCode,
            this.colMaterialName,
            this.colGrossWeight,
            this.colTare,
            this.colNetWeight,
            this.colWeighingBy,
            this.colBalanceCode,
            this.colWeighingDate});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colWeighingID
            // 
            this.colWeighingID.Caption = "Weighing ID";
            this.colWeighingID.FieldName = "WeighingID";
            this.colWeighingID.Name = "colWeighingID";
            this.colWeighingID.Visible = true;
            this.colWeighingID.VisibleIndex = 0;
            // 
            // colProductLN
            // 
            this.colProductLN.Caption = "Product Lot No";
            this.colProductLN.FieldName = "ProductLN";
            this.colProductLN.Name = "colProductLN";
            this.colProductLN.Visible = true;
            this.colProductLN.VisibleIndex = 1;
            // 
            // colProductExpDate
            // 
            this.colProductExpDate.Caption = "Product Exp.Date";
            this.colProductExpDate.FieldName = "ProductExpDate";
            this.colProductExpDate.Name = "colProductExpDate";
            this.colProductExpDate.Visible = true;
            this.colProductExpDate.VisibleIndex = 2;
            // 
            // colProductName
            // 
            this.colProductName.Caption = "Product Name";
            this.colProductName.FieldName = "ProductName";
            this.colProductName.Name = "colProductName";
            this.colProductName.Visible = true;
            this.colProductName.VisibleIndex = 3;
            // 
            // colMaterialLN
            // 
            this.colMaterialLN.Caption = "Material Lot No";
            this.colMaterialLN.FieldName = "MaterialLN";
            this.colMaterialLN.Name = "colMaterialLN";
            this.colMaterialLN.Visible = true;
            this.colMaterialLN.VisibleIndex = 13;
            // 
            // colMaterialExpDate
            // 
            this.colMaterialExpDate.Caption = "Material Exp.Date";
            this.colMaterialExpDate.FieldName = "MaterialExpDate";
            this.colMaterialExpDate.Name = "colMaterialExpDate";
            this.colMaterialExpDate.Visible = true;
            this.colMaterialExpDate.VisibleIndex = 4;
            // 
            // colMaterialCode
            // 
            this.colMaterialCode.Caption = "Code";
            this.colMaterialCode.FieldName = "MaterialCode";
            this.colMaterialCode.Name = "colMaterialCode";
            this.colMaterialCode.Visible = true;
            this.colMaterialCode.VisibleIndex = 5;
            // 
            // colMaterialName
            // 
            this.colMaterialName.Caption = "Material Name";
            this.colMaterialName.FieldName = "MaterialName";
            this.colMaterialName.Name = "colMaterialName";
            this.colMaterialName.Visible = true;
            this.colMaterialName.VisibleIndex = 6;
            // 
            // colGrossWeight
            // 
            this.colGrossWeight.Caption = "Gross Weight";
            this.colGrossWeight.FieldName = "GrossWeight";
            this.colGrossWeight.Name = "colGrossWeight";
            this.colGrossWeight.Visible = true;
            this.colGrossWeight.VisibleIndex = 7;
            // 
            // colTare
            // 
            this.colTare.Caption = "Tare";
            this.colTare.FieldName = "Tare";
            this.colTare.Name = "colTare";
            this.colTare.Visible = true;
            this.colTare.VisibleIndex = 8;
            // 
            // colNetWeight
            // 
            this.colNetWeight.Caption = "Net Weight";
            this.colNetWeight.FieldName = "NetWeight";
            this.colNetWeight.Name = "colNetWeight";
            this.colNetWeight.Visible = true;
            this.colNetWeight.VisibleIndex = 9;
            // 
            // colWeighingBy
            // 
            this.colWeighingBy.Caption = "Weighing By";
            this.colWeighingBy.FieldName = "WeighingBy";
            this.colWeighingBy.Name = "colWeighingBy";
            this.colWeighingBy.Visible = true;
            this.colWeighingBy.VisibleIndex = 10;
            // 
            // colBalanceCode
            // 
            this.colBalanceCode.Caption = "Balance Code";
            this.colBalanceCode.FieldName = "BalanceCode";
            this.colBalanceCode.Name = "colBalanceCode";
            this.colBalanceCode.Visible = true;
            this.colBalanceCode.VisibleIndex = 11;
            // 
            // colWeighingDate
            // 
            this.colWeighingDate.Caption = "Weighing Date";
            this.colWeighingDate.FieldName = "WeighingDate";
            this.colWeighingDate.Name = "colWeighingDate";
            this.colWeighingDate.Visible = true;
            this.colWeighingDate.VisibleIndex = 12;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1047, 467);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1027, 447);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.simpleButtonExportExcel);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.layoutControl2.Location = new System.Drawing.Point(0, 485);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.layoutControlGroup1;
            this.layoutControl2.Size = new System.Drawing.Size(1047, 47);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup1.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 10D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 90D;
            this.layoutControlGroup1.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2});
            rowDefinition1.Height = 100D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            this.layoutControlGroup1.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1});
            this.layoutControlGroup1.Size = new System.Drawing.Size(1047, 47);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // simpleButtonExportExcel
            // 
            this.simpleButtonExportExcel.Location = new System.Drawing.Point(12, 12);
            this.simpleButtonExportExcel.Name = "simpleButtonExportExcel";
            this.simpleButtonExportExcel.Size = new System.Drawing.Size(99, 22);
            this.simpleButtonExportExcel.StyleController = this.layoutControl2;
            this.simpleButtonExportExcel.TabIndex = 4;
            this.simpleButtonExportExcel.Text = "Export Excel";
            this.simpleButtonExportExcel.Click += new System.EventHandler(this.simpleButtonExportExcel_Click);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonExportExcel;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(103, 27);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // RecordListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 532);
            this.Controls.Add(this.layoutControl2);
            this.Controls.Add(this.layoutControl1);
            this.Name = "RecordListForm";
            this.Text = "RecordListForm";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colWeighingID;
        private DevExpress.XtraGrid.Columns.GridColumn colProductLN;
        private DevExpress.XtraGrid.Columns.GridColumn colProductExpDate;
        private DevExpress.XtraGrid.Columns.GridColumn colProductName;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialLN;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialExpDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialCode;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialName;
        private DevExpress.XtraGrid.Columns.GridColumn colGrossWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colTare;
        private DevExpress.XtraGrid.Columns.GridColumn colNetWeight;
        private DevExpress.XtraGrid.Columns.GridColumn colWeighingBy;
        private DevExpress.XtraGrid.Columns.GridColumn colBalanceCode;
        private DevExpress.XtraGrid.Columns.GridColumn colWeighingDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonExportExcel;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}