﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeighingWebService.Models
{
    public class ProductModel
    {
        public string LotNo { get; set; }
        public string ProductName { get; set; }
        public DateTime? ExpDate { get; set; }
    }
}
