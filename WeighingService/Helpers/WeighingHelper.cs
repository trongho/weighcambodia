﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;
using WeighingWebService.Models;

namespace WeighingWebService.Helpers
{
    public class WeighingHelper
    {
        public static List<WeighingModel> Covert(List<Weighing> entrys)
        {
            var models = entrys.ConvertAll(sc => new WeighingModel
            {
                WeighingID=sc.WeighingID,
                ProductLN=sc.ProductLN,
                ProductExpDate=sc.ProductExpDate,
                ProductName=sc.ProductName,
                MaterialLN=sc.MaterialLN,
                MaterialExpDate=sc.MaterialExpDate,
                MaterialCode=sc.MaterialCode,
                MaterialName=sc.MaterialName,
                GrossWeight=sc.GrossWeight,
                Tare=sc.Tare,
                NetWeight=sc.NetWeight,
                WeighingBy=sc.WeighingBy,
                BalanceCode=sc.BalanceCode,
                WeighingDate=sc.WeighingDate,
            });

            return models;
        }
    }
}
