﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;
using WeighingWebService.Models;

namespace WeighingWebService.Helpers
{
    public class ProductHelper
    {
        public static List<ProductModel> Covert(List<Product> entrys)
        {
            var models = entrys.ConvertAll(sc => new ProductModel
            {
                LotNo = sc.LotNo,
                ProductName=sc.ProductName,
                ExpDate = sc.ExpDate,
            });

            return models;
        }
    }
}
