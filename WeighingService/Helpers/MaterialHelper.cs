﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;
using WeighingWebService.Models;

namespace WeighingWebService.Helpers
{
    public class MaterialHelper
    {
        public static List<MaterialModel> Covert(List<Material> entrys)
        {
            var models = entrys.ConvertAll(sc => new MaterialModel
            {
                LotNo=sc.LotNo,
                MaterialName=sc.MaterialName,
                Code=sc.Code,
                ExpDate=sc.ExpDate,
            });

            return models;
        }
    }
}
