﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;
using WeighingWebService.Helpers;
using WeighingWebService.Interfaces;

namespace WeighingWebService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService service;

        public ProductController(IProductService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = ProductHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetUnderId(String id)
        {
            var entrys = await service.GetUnderId(id);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = ProductHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Product product)
        {
            var entry = await service.Create(product);

            return CreatedAtAction(
                 nameof(Get), new { id = product.LotNo }, entry);
        }
    }
}
