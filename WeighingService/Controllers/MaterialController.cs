﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;
using WeighingWebService.Helpers;
using WeighingWebService.Interfaces;

namespace WeighingWebService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MaterialController :ControllerBase
    {
        private readonly IMaterialService service;

        public MaterialController(IMaterialService service)
        {
            this.service=service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = MaterialHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetUnderId(String id)
        {
            var entrys = await service.GetUnderId(id);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = MaterialHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Material material)
        {
            var entry = await service.Create(material);

            return CreatedAtAction(
                 nameof(Get), new { id = material.LotNo }, entry);
        }

    }
}
