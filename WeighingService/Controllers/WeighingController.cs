﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;
using WeighingWebService.Helpers;
using WeighingWebService.Interfaces;

namespace WeighingWebService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeighingController : ControllerBase
    {
        private readonly IWeighingService service;

        public WeighingController(IWeighingService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WeighingHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetUnderId(String id)
        {
            var entrys = await service.GetUnderId(id);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WeighingHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Weighing weighing)
        {
            var entry = await service.Create(weighing);

            return CreatedAtAction(
                 nameof(Get), new { id = weighing.WeighingID }, entry);
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> Delete()
        {
            var entry = await service.Delete();
            return Ok(entry);
        }

        [HttpGet]
        [Route("GetID")]
        public async Task<IActionResult> GetID()
        {
            var entry = await service.GetID();
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }
    }
}
