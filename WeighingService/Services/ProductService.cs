﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;
using WeighingWebService.Interfaces;

namespace WeighingWebService.Services
{
    public class ProductService : IProductService
    {
        private readonly WeighDBContext context;
        public ProductService(WeighDBContext context)
        {
            this.context = context;
        }

        public async Task<bool> Create(Product product)
        {
            var entry = context.Products.AddAsync(product);
            context.SaveChanges();

            return entry.IsCompleted;
        }

        public async Task<List<Product>> GetAll()
        {
            var entrys = context.Products;
            return await entrys.ToListAsync();
        }

        public async Task<List<Product>> GetUnderId(string id)
        {
            var entry = context.Products.Where(u => u.LotNo.Equals(id));
            return await entry.ToListAsync();
        }
    }
}
