﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;
using WeighingWebService.Interfaces;

namespace WeighingWebService.Services
{
    public class MaterialService : IMaterialService
    {
        private readonly WeighDBContext context;
        public MaterialService(WeighDBContext context)
        {
            this.context = context;
        }

        public async Task<bool> Create(Material material)
        {
            var entry = context.Materials.AddAsync(material);
            context.SaveChanges();
            return entry.IsCompleted;
        }

        public async Task<List<Material>> GetAll()
        {
            var entrys = context.Materials;
            return await entrys.ToListAsync();
        }

        public async Task<List<Material>> GetUnderId(string id)
        {
            var entry = context.Materials.Where(u => u.LotNo.Equals(id));
            return await entry.ToListAsync();
        }
    }
}
