﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;
using WeighingWebService.Interfaces;

namespace WeighingWebService.Services
{
    public class WeighingService : IWeighingService
    {
        private readonly WeighDBContext context;
        public WeighingService(WeighDBContext context)
        {
            this.context = context;
        }
        public async Task<bool> Create(Weighing weighing)
        {
            var entry = context.Weighings.AddAsync(weighing);
            context.SaveChanges();

            return entry.IsCompleted;
        }

        //public int getID()
        //{
        //    int id = 0;
        //    String lastID = context.Weighings.OrderByDescending(x => x.WeighingID).Take(1).Select(x => x.WeighingID).ToList().FirstOrDefault();
        //    if (lastID == null)
        //    {
        //        id = 1;
        //    }
        //    else
        //    {
        //        id = int.Parse(lastID) + 1;
        //    }
        //    return id;
        //}

        public async Task<bool> Delete()
        {
            var entry = context.Weighings;
            context.Weighings.RemoveRange(entry);
            context.SaveChanges();
            return true;
        }

        public async Task<List<Weighing>> GetAll()
        {
            var entrys = context.Weighings;
            return await entrys.ToListAsync();
        }

        public async Task<List<Weighing>> GetUnderId(string id)
        {
            var entry = context.Weighings.Where(u => u.WeighingID.Equals(id));
            return await entry.ToListAsync();
        }

        public async Task<string> GetID()
        {
            int id = 0;
            string lastID =context.Weighings.OrderByDescending(x => x.WeighingID).Take(1).Select(x => x.WeighingID).ToList().FirstOrDefault();
            if (lastID == null)
            {
                id = 1;
            }
            else
            {
                id =int.Parse(lastID) + 1;
            }
            return id.ToString();
        }
    }
}
