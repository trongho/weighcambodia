﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeighingWebService.Entitys
{
    public class Product
    {
        public string LotNo { get; set; }
        public string ProductName { get; set; }
        public DateTime? ExpDate { get; set; }
    }
}
