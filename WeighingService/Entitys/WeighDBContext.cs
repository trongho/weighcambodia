﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeighingWebService.Entitys
{
    public class WeighDBContext : DbContext
    {
        public DbSet<Weighing> Weighings { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<Product> Products { get; set; }

        public WeighDBContext(DbContextOptions<WeighDBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
           
            modelBuilder.Entity<Weighing>().ToTable("Weighing").HasKey(e => e.WeighingID);
            modelBuilder.Entity<Weighing>().Property(p => p.GrossWeight).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Weighing>().Property(p => p.NetWeight).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Weighing>().Property(p => p.Tare).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<Material>().ToTable("Material").HasNoKey();

            modelBuilder.Entity<Product>().ToTable("Product").HasNoKey();
        }

    }
}
