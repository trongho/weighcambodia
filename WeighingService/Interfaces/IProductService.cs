﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;

namespace WeighingWebService.Interfaces
{
    public interface IProductService
    {
        Task<List<Product>> GetAll();
        Task<List<Product>> GetUnderId(String id);
        Task<Boolean> Create(Product product);
    }
}
