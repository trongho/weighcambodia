﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;

namespace WeighingWebService.Interfaces
{
    public interface IWeighingService
    {
        Task<List<Weighing>> GetAll();
        Task<List<Weighing>> GetUnderId(String id);
        Task<Boolean> Create(Weighing weighing);
        Task<Boolean> Delete();
        Task<String> GetID();
    }
}
