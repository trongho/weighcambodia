﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeighingWebService.Entitys;

namespace WeighingWebService.Interfaces
{
    public interface IMaterialService
    {
        Task<List<Material>> GetAll();
        Task<List<Material>> GetUnderId(String id);
        Task<Boolean> Create(Material material);
    }
}
